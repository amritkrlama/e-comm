# E-Comm (INCTURE)
## The Application is running on port 8900.   
## The Application uses in-memory database (H2). The console is accessible at (http://localhost:8900/h2-console)

# API Details:

**Item:**  
-`GET`: /item (http://localhost:8900/item): Lists all the items present.

-`GET`: /item/{id} (http://localhost:8900/item/1001): Lists the item with the id passed.

-`POST`: /item (http://localhost:8900/item): Adds the item passed.  
	Sample request:  
```
	{
        "name": "Bike",
        "brand": "Yamaha",
        "quantityLeft": 40,
        "price": 200000
    } 
```
	
-`DELETE`:  /item (http://localhost:8900/item): Deletes the item passed  
	Sample request:
```	
	{
        "name": "Bike",
        "brand": "Yamaha",
        "quantityLeft": 40,
        "price": 200000
    }
```	

-`PUT`:   /item (http://localhost:8900/item): Deletes the item passed  
	Sample request:
```
	{
    "id": 1,
    "name": "Bike",
    "brand": "Yamaha",
    "quantityLeft": 20,
    "price": 200000
	}	
```	
	
	
**Order:**  
`GET`: /order (http://localhost:8900/order): Lists all the orders present.

`POST`: /order (http://localhost:8900/item): Add the order passed.  
	Sample request:  
```
	{
        "emailId": "amritkrlama@yahoo.com",
        "quantityOrdered": 5,
        "items": {
            "id": 1001
        }
 	 }
```

`POST`: /multipleOrder (localhost:8900/multipleOrder): Add multiple orders  
     Sample request:  
```
	[
		{
			"emailId": "amritkrlama@yahoo.com",
			"quantityOrdered": 5,
			"items": {
				"id": 1001
			}
		 },
		 {
			"emailId": "amritkrlama@hotmail.com",
			"quantityOrdered": 15,
			"items": {
				"id": 1002
			}
		 }
	 ]
```	 
	 
**Sample error response for out of stock:**
```
{
    "status": "Item out of stock. Item: Phone"
}
```
