package com.incture;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication(scanBasePackages="com.incture")
@EntityScan(basePackages="com.incture")
public class ECommApplication {

	public static void main(String[] args) {
		SpringApplication.run(ECommApplication.class, args);
	}

}
