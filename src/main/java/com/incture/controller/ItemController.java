package com.incture.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.incture.entity.Item;
import com.incture.service.ItemService;

@RestController
public class ItemController {
	
	@Autowired
	private ItemService itemService;
	
	@GetMapping(value="/item")
	public List<Item> getAllItems(){
		return itemService.getAllItems();
	}
	
	@GetMapping(value="/item/{id}")
	public Item getItemById(@PathVariable Long id) {
		return itemService.findById(id);
	}
	
	@PostMapping(value="/item")
	public Item addItem(@RequestBody Item item) {
		return itemService.addItem(item);
	}
	
	@DeleteMapping(value="/item")
	public void deleteItem(@RequestBody Item item){
		itemService.deleteItem(item);
	}
	
	@PutMapping(value="/item")
	public Item updateItem(@RequestBody Item item) {
		return itemService.updateItem(item);
	}
}
