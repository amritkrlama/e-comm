package com.incture.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.incture.entity.Orders;
import com.incture.exception.ItemOutOfStockException;
import com.incture.service.OrderService;

@RestController
public class OrderController {
	
	@Autowired
	private OrderService orderService;

	@GetMapping("/order")
	public List<Orders> getAllOrders(){
		return orderService.getAllOrders();
	}
	
	@PostMapping("/order")
	public Orders makeAnOrder(@RequestBody Orders order) throws ItemOutOfStockException {
		return orderService.addOrder(order);
	}
	
	@PostMapping("/multipleOrder")
	public List<Orders> makeMultipleOrder(@RequestBody List<Orders> orders) throws ItemOutOfStockException{
		return orderService.addMultipleOrders(orders);
	}
}
