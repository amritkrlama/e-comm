package com.incture.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Item {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String name;
	
	private String brand;
	
	private int quantityLeft;
	
	private int price;
	
	@JsonIgnore
	@OneToMany(mappedBy="item")
	private List<Orders> orders = new ArrayList<>();
	
	public Item() {
		super();
	}


	public Item(String name, String brand, int quantityLeft, int price, List<Orders> Orders) {
		super();
		this.name = name;
		this.brand = brand;
		this.quantityLeft = quantityLeft;
		this.price = price;
		this.orders = Orders;
	}



	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getBrand() {
		return brand;
	}


	public void setBrand(String brand) {
		this.brand = brand;
	}


	public int getPrice() {
		return price;
	}


	public void setPrice(int price) {
		this.price = price;
	}

	public Long getId() {
		return id;
	}
	
	
	public List<Orders> getOrders() {
		return orders;
	}

	public void addOrders(Orders Orders) {
		this.orders.add(Orders);
	}
	
	public void removeOrders(Orders Orders) {
		this.orders.remove(Orders);
	}
	

	public int getQuantityLeft() {
		return quantityLeft;
	}

	public void setQuantityLeft(int quantityLeft) {
		this.quantityLeft = quantityLeft;
	}

	@Override
	public String toString() {
		return "Item [id=" + id + ", name=" + name + ", brand=" + brand + ", quantity=" + quantityLeft + ", price=" + price
				+ "]";
	}
	
	
	
	
}
