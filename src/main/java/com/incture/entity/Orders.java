package com.incture.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Orders {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne
	private Item item;
	
	private String emailId;

	private int quantityOrdered;


	public Orders(Item item, String emailId, int quantityOrdered) {
		super();
		this.item = item;
		this.emailId = emailId;
		this.quantityOrdered = quantityOrdered;
	}

	public Orders() {
		super();
	}

	public Long getOrderId() {
		return id;
	}

	public Item getItems() {
		return item;
	}

	public void setItems(Item items) {
		this.item = items;
	}


	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	

	public int getQuantityOrdered() {
		return quantityOrdered;
	}

	public void setQuantityOrdered(int quantityOrdered) {
		this.quantityOrdered = quantityOrdered;
	}

	@Override
	public String toString() {
		return "Orders [item=" + item + ", emailId=" + emailId + ", quantityOrdered=" + quantityOrdered + "]";
	}

	
	
}
