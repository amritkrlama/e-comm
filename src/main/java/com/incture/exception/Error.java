package com.incture.exception;

import org.springframework.stereotype.Component;

@Component
public class Error {

	private String status;
	
	public Error() {
		
	}

	public Error(String status) {
		super();
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
