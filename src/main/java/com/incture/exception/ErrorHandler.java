package com.incture.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice
@RestController
public class ErrorHandler {
	
	@ExceptionHandler(ItemOutOfStockException.class)
	public final ResponseEntity<Error> handleError(ItemOutOfStockException exception){
		Error error = new Error("Item out of stock. Item: "+exception.getLocalizedMessage());
		return new ResponseEntity<>(error,HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
