package com.incture.exception;

public class ItemOutOfStockException extends Exception{
	
	private static final long serialVersionUID = 1L;
	
	public ItemOutOfStockException(String message) {
		super(message);
	}
	
	public ItemOutOfStockException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public ItemOutOfStockException() {
		
	}
}
