package com.incture.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.incture.entity.Item;

@Repository
@Transactional
public class ItemRepository {

	public final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	public List<Item> getAllItems(){
		Query queryForAllItems = em.createQuery("SELECT i FROM Item i", Item.class);
		return queryForAllItems.getResultList();
	}
	
	
	public Item addItem(Item item) {
		logger.info("Received Item to add {}", item);
		em.persist(item);
		return item;
	}
	
	
	public Item deleteItem(Item item) {
		logger.info("Received Item to delete {}", item);
		em.remove(item);
		return item;
	}
	
	
	public Item findById(Long id) {
		return em.find(Item.class, id);
	}
	
	
	public Item updateItem(Item item) {
		logger.info("Received Item to update {}", item);
		return em.merge(item);
	}
}
