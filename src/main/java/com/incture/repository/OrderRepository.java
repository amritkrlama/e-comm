package com.incture.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.incture.entity.Orders;

@Repository
public interface OrderRepository extends JpaRepository<Orders, Long>{
	
}
