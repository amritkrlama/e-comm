package com.incture.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incture.entity.Item;
import com.incture.repository.ItemRepository;

@Service
public class ItemService {

	public final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ItemRepository repository;
	
	public List<Item> getAllItems(){
		return repository.getAllItems();
	}
	
	
	public Item addItem(Item item) {
		return repository.addItem(item);
	}
	
	
	public Item deleteItem(Item item) {
		return repository.deleteItem(item);
	}
	
	
	public Item findById(Long id) {
		return repository.findById(id);
	}
	
	
	public Item updateItem(Item item) {
		return repository.updateItem(item);
	}
}
