package com.incture.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incture.entity.Item;
import com.incture.entity.Orders;
import com.incture.exception.ItemOutOfStockException;
import com.incture.repository.OrderRepository;

@Service
public class OrderService {
	
	public final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private ItemService itemService;
	
	
	public List<Orders> getAllOrders(){
		return orderRepository.findAll();
	}
	
	public Orders addOrder(Orders order) throws ItemOutOfStockException {
		int quantityOrdered = order.getQuantityOrdered();
		Item item = order.getItems();
		Long itemId = item.getId();
		Item actualItem = itemService.findById(itemId);
		logger.info("Quantity Remaining {}", actualItem.getQuantityLeft());
		if(quantityOrdered>actualItem.getQuantityLeft()) {
			throw new ItemOutOfStockException(actualItem.getName());
		}
		actualItem.setQuantityLeft(actualItem.getQuantityLeft()-quantityOrdered);
		order.setItems(actualItem);
		return orderRepository.save(order);
	}
	
	
	public List<Orders> addMultipleOrders(List<Orders> orders) throws ItemOutOfStockException{
		List<Orders> finalOrders = new ArrayList<>();
		for(Orders order: orders){
			int quantityOrdered = order.getQuantityOrdered();
			Item item = order.getItems();
			Long itemId = item.getId();
			Item actualItem = itemService.findById(itemId);
			if(quantityOrdered>actualItem.getQuantityLeft()) {
				throw new ItemOutOfStockException(actualItem.getName());
			}
			actualItem.setQuantityLeft(actualItem.getQuantityLeft()-quantityOrdered);
			order.setItems(actualItem);
			finalOrders.add(order);
		}
		return orderRepository.saveAll(orders);
	}
	
}
